class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
	render html: "\u00A1Hola, mundo!"
  end

  def goodbye
     	render html: "goodbye, world!"
  end

end
